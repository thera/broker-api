import sys




def generate_new_template(ENVNameParam):
    with open('cf-raw.json', 'r') as file:
        data = file.read().replace('${pyENVNameParam}', ENVNameParam)
        with open("cf-template.json", "w") as output:
            output.write(data)

if __name__ == "__main__":
    generate_new_template(sys.argv[1])