deploy:
	cp src/broker/index.js dist/broker/
	cp src/broker/package.json dist/broker/
	cd dist/broker/; npm i --arch=x64 --platform=linux --target=8.10.0
	python pre-compiler.py DEV
	aws cloudformation  package --template cf-template.json --s3-bucket functions-gs --output-template cf-template-export.json --profile gs
	aws cloudformation deploy --template-file cf-template-export.json --stack-name brokerDEV --capabilities CAPABILITY_NAMED_IAM --tags Environment=DEV Project=broker Module=broker --parameter-overrides EnvNameParam=dev ENVNameParam=DEV --profile gs
