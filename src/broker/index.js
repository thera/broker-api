const AWS = require('aws-sdk');

const atob = require('atob');


exports.handler = async (event) => {
    for (let { messageId, body } of event.Records) {
        console.log('SQS message %s: %j', messageId, body);
            
        body = atob(body);
        var dynamodb = new AWS.DynamoDB.DocumentClient({region: 'us-east-1'});
        
        const json = JSON.parse(body);
        
        
        const tables = {
            "payments": "Payments",
            "transactions": "Transactions"
        }
        
        
        let batch = {}
        
        if (json.payments) {
            
            batch = {
                "Payments": [
                ]
            }
            
            json.payments.forEach((payment) => {
                batch.Payments.push({
                    PutRequest: {
                        Item: payment
                    }   
                })
                
            });
            
        } else {

            batch = {
                "Transactions": [
                ]
            }
            
            json.transactions.forEach((transaction) => {
                batch.Transactions.push({
                    PutRequest: {
                        Item: transaction
                    }   
                })
                
            });
            
        }
       
        const params = {
            RequestItems: batch
        }
        
        
        
        
        return new Promise((resolve, reject)=>{
            dynamodb.batchWrite(params, function(err, data) {
                if(err) {
                    reject(err)
                }
                
                resolve(true)
            })
        })
        
    }
};
